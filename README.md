This project tests the functioning of Weasyprint for any given HTML file or website URL input. It uses Docker and the resulting image is based on Alpine. Python 3.7 is installed together with the base Weasyprint dependencies. 

[Weasyprint](https://weasyprint.org/) helps web developers create PDF documents. Weasyprint is a Python application.


## Why this project
After trying many methods of saving a simple HTML page as a pdf document I have found shortcomings in nearly all the tools. After testing browser extensions, client side javascript tools, and various Python applications none of them can supply all the required functions.

Shortcomings I have found in the generated pdfs:
- URLs are not transcribed to be clickable
- CSS pagebreaks are not respected
- Page format is not handled and general layout is not respeted
- No pdf content index is created


## Usage
The Dockerfile contains some notes, mainly as I like to leave a record so when returning after some time away there is something to grab hold of.

To build from this Dockerfile: 
`docker build -t weleoka/weasyprint:latest .`

To run the resulting image:
```
docker run -it --rm --name weasyprint -v `pwd`:/app -w /app --network host weleoka/weasyprint:latest` 
```
This will run with interactive and tty, remove the container when exiting, name the container, mount the volume of current dir `pwd` in the container's `/app` dir, and finally `host` networking so the container runs on the host network and not a docker proxy. Then is of course the image tag to actually run. finish the above command off with input source and output destination pdf.


## Improvements to this project
Turn the projet into a service by having weasyprint behind a HTTP server that accepts a POST request containing the url for the page and returning the generated pdf file. See this solution at asdfasdfas on dockerhub.


## Usefull resources
If you are looking for the best solution to converting an HTML document into a pdf file I have found that the client side javascript application at [Html2pdf.js](https://github.com/sonnn/html2pdf.js) to be the best. 

It makes sense to me that ultimately the solution has to be javascript based for the web to have access to everything at rendering time. The only thing I would now like to see is html2pdf.js creating an index in the resulting pdf like Weasyprint does!