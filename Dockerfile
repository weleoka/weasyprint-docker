FROM python:3.7-alpine3.10

ARG version=48

RUN apk update
RUN apk upgrade 
RUN apk add python3-dev gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev
RUN pip install weasyprint==$version

ENTRYPOINT [ "weasyprint" ]